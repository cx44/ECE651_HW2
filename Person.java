public class Person {

	protected String firstname;
	protected String lastname;
	protected String gender;

	public static void main(String[] args) {
	}
}

class BlueDevil extends Person {
	private String workexp;// sufficient or not
	private String major;
	private String title;

	public void whoIs(String firstname, String lastname, BlueDevil[] p) {
		boolean mark = false;
		for (int i = 0; i < p.length; i++) {
			if ((firstname.equals(p[i].firstname)) && (lastname.equals(p[i].lastname))) {
				System.out.println(p[i].firstname + " " + p[i].lastname + " is a " + p[i].title + " in " + p[i].major
						+ ", " + p[i].gender + " work experience is " + p[i].workexp + ".");
				mark = true;
				break;
			}
		}
		if (mark == false) {
			System.out.println("person does not exist for whois");
		}
	}

	public void searchTA(String fn, String ln, BlueDevil[] p) {
		boolean mark = false;
		for (int i = 3; i < p.length; i++) {
			if ((firstname.equals(p[i].firstname)) && (lastname.equals(p[i].lastname))) {
				System.out.println(p[i].firstname + " " + p[i].lastname + " is available as your TA");
				mark = true;
				break;
			}
		}
		if (mark == false) {
			System.out.println("person does not exist for searchTA");
		}
	}

	public BlueDevil(String fname, String lname) {
		this.firstname = fname;
		this.lastname = lname;
	}

	public static void main(String[] args) {

		BlueDevil[] info = new BlueDevil[9];
		info[0] = new BlueDevil("Ric", "Telford");
		info[1] = new BlueDevil("Adel", "Fahmy");
		info[2] = new BlueDevil("Chong", "Xu");
		info[3] = new BlueDevil("Yuanyuan", "Yu");
		info[4] = new BlueDevil("You", "Lyu");
		info[5] = new BlueDevil("Sihao", "Yao");
		info[6] = new BlueDevil("Lei", "Chen");
		info[7] = new BlueDevil("Shalin", "Shah");
		info[8] = new BlueDevil("Zhongyu", "Li");
		info[0].gender = "his";
		info[1].gender = "his";
		info[2].gender = "her";
		info[3].gender = "her";
		info[4].gender = "whose";
		info[5].gender = "whose";
		info[6].gender = "her";
		info[7].gender = "his";
		info[8].gender = "whose";
		info[0].workexp = "sufficient";
		info[1].workexp = "sufficient";
		info[2].workexp = "little";
		info[3].workexp = "unknown";
		info[4].workexp = "unknown";
		info[5].workexp = "unknown";
		info[6].workexp = "unknown";
		info[7].workexp = "unknown";
		info[8].workexp = "unknown";
		info[0].major = "ECE";
		info[1].major = "ECE";
		info[2].major = "ECE";
		info[3].major = "ECE";
		info[4].major = "ECE";
		info[5].major = "ECE";
		info[6].major = "ECE";
		info[7].major = "ECE";
		info[8].major = "ECE";
		info[0].title = "professor";
		info[1].title = "professor";
		info[2].title = "student";
		info[3].title = "student";
		info[4].title = "student";
		info[5].title = "student";
		info[6].title = "student";
		info[7].title = "student";
		info[8].title = "student";
		BlueDevil BD = new BlueDevil(args[0], args[1]);
		if (args[2].contentEquals("whois")) {
			BD.whoIs(BD.firstname, BD.lastname, info);
		} else if (args[2].contentEquals("searchTA")) {
			BD.searchTA(BD.firstname, BD.lastname, info);
		} else {
			System.out.println("command not found");
		}
	}

}
