# ECE651_HW2

hw2 of ECE651

compile: run Person.java with javac through proper path

javac -d bin -sourcepath src src/Person.java

run: run BlueDevil arg[0] arg[1] arg[2] with java, in which arg[i] is an argument you put in

java BlueDevil arg[0] arg[1] arg[2]

My program takes in 3 arguments, the first is first name of the person you want to search,the
second is the last name of the person you want to search, the last is the type of search.

There are 2 types of searches, you can input "whois" for the last argument, which will lead you
to searching an array of objects by first name and last name and will return information of a 
specific person or error warning if this person does not exist.

You can also input "searchTA" for the argument, so you can only search for TAs, if he or she is a
TA, you will see "name is available as your TA". Searching other people can lead to error.

What is more, inputing other commands can lead to error warning, too.